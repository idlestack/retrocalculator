//
//  ViewController.swift
//  RetroCalculator
//
//  Created by Douglas Spencer on 9/9/16.
//  Copyright © 2016 Douglas Spencer. All rights reserved.
//

import UIKit
import AVFoundation;

class ViewController: UIViewController {

    var btnSound: AVAudioPlayer!
    
    
    enum Operation: String {
        case divide = "/"
        case multiply = "*"
        case subtract = "-"
        case add = "+"
        case Empty = "Empty"
    }
    
    @IBOutlet weak var OutputLabel: UILabel!
    
    var runningNumber = ""
    var CurrentOperation = Operation.Empty
    var leftValStr = ""
    var rightValStr = ""
    var result = ""
    
    override func motionEnded(_ motion: UIEventSubtype, with event: UIEvent?) {
        if motion == .motionShake
        {
            runningNumber = ""
            CurrentOperation = Operation.Empty
            leftValStr = ""
            rightValStr = ""
            result = ""
            OutputLabel.text = "0"
            
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //OutputLabel.text = ""
        
        let path = Bundle.main.path(forResource: "btn", ofType: "wav")
        let PowerUpPath = Bundle.main.path(forResource: "PowerUp", ofType: "wav")
        let soundURL = URL(fileURLWithPath: path!)
        let PowerUpURL = URL(fileURLWithPath: PowerUpPath!)
        
        do {
            try btnSound = AVAudioPlayer(contentsOf: soundURL)
            btnSound.prepareToPlay()
        } catch let err as NSError {
            print(err.debugDescription)
        }
    }
    
    @IBAction func numberPress(sender:  UIButton) {
        playSound()
        runningNumber += "\(sender.tag)"
        OutputLabel.text = runningNumber
    }
    
    @IBAction func onDividePressed(sender: AnyObject){
        ProcessOperation(operation: Operation.divide)
    }
    @IBAction func onMultiplyPressed(sender: AnyObject){
        ProcessOperation(operation: Operation.multiply)
    }
    @IBAction func onSubtractPressed(sender: AnyObject){
        ProcessOperation(operation: Operation.subtract)
    }
    @IBAction func onAddPressed(sender: AnyObject){
        ProcessOperation(operation: Operation.add)
    }
    @IBAction func onEqual(sender: AnyObject){
        ProcessOperation(operation: CurrentOperation)
    }
    
    
    
    
    func playSound()  {
        if btnSound.isPlaying {
            btnSound.stop()
        }
        
        btnSound.play()
    }
    
        
    func ProcessOperation(operation: Operation) {
        playSound()
        
        if ( CurrentOperation != Operation.Empty){
            if  runningNumber != "" {
                rightValStr = runningNumber
                runningNumber = ""
                
                if CurrentOperation == Operation.multiply
                {
                    result = "\(Double(leftValStr)! * Double(rightValStr)!)"
                }
                else if CurrentOperation == Operation.divide
                {
                    result = "\(Double(leftValStr)! / Double(rightValStr)!)"

                }
                else if CurrentOperation == Operation.subtract
                {
                    result = "\(Double(leftValStr)! - Double(rightValStr)!)"

                }
                else  if CurrentOperation == Operation.add
                {
                    result = "\(Double(leftValStr)! + Double(rightValStr)!)"

                }
                
                leftValStr = result
                OutputLabel.text = result
        }
        
            CurrentOperation = operation
    }
        else
        {
            leftValStr = runningNumber
            runningNumber = ""
            CurrentOperation = operation
        }

}

}
